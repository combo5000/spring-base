package be.oml.run;

import be.oml.controller.AttackController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class BaseApplicationTest {

    @Autowired
    private AttackController controller;

    @Test
    void contextLoad() {
        assertThat(controller).isNotNull();
    }
}
