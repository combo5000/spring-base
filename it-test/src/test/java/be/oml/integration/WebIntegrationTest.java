package be.oml.integration;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.when;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebIntegrationTest {

    @LocalServerPort
    private int port;

    public String BASE_URL;
    public static final String ATTACK_API = "/api/attack";

    @BeforeEach
    void setUp() {
        BASE_URL = "http://localhost:" + port;
    }

    @Test
    void testAttackCommands() {
        // Arrange

        // Act
        final String response = when()
                .get(BASE_URL + ATTACK_API)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .asString();

        // Assert
        assertThat(response).contains("try");
    }
}
