package be.oml.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Create a duplicate of the application context because we are unable to get the appContext from run module
 */
@SpringBootApplication(scanBasePackages = "be.oml")
public class TestApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }
}
