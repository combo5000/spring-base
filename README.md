# Spring Boot Base

Example how to setup maven multi module setup

- run
    - api
    - service
    - core
  
## How to run
run `gradle build`

### local
`gradle :run:bootRun`

### docker
`gradle :run:dockerBuildImage && docker-compose up -d`
