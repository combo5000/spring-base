package be.oml.service;

import be.oml.dto.PunchDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class HitServiceTest {

    private HitService sut;

    @BeforeEach
    void setUp() {
        sut = new HitService();
    }

    @Test
    void throwPunchForUser() {
        // Arrange
        final String user = "user";
        // Act
        final PunchDto punchDto = sut.throwPunchForUser(user);

        // Assert
        assertThat(punchDto)
                .isNotNull()
                .extracting(PunchDto::getOwner).isEqualTo(user);
    }

    @Test
    void throwPunchForEmptyUser() {
        // Act
        final PunchDto punchDto = sut.throwPunchForUser(null);

        // Assert
        assertThat(punchDto).isNull();
    }
}
