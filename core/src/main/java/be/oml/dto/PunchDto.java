package be.oml.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PunchDto {
    private final Long power;
    private final Long speed;
    private final String owner;
}
