package be.oml.service;

import be.oml.Punch;
import be.oml.dto.PunchDto;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;

@Service
public class HitService {

    public PunchDto throwPunchForUser(final String user) {
        if (isNull(user)) {
            return null;
        }
        final Punch punch = new Punch(2, 5);

        return PunchDto.builder()
                .speed(punch.getSpeed())
                .power(punch.getPower())
                .owner(user)
                .build();
    }

}
