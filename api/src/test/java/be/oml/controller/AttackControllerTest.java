package be.oml.controller;

import be.oml.dto.PunchDto;
import be.oml.service.HitService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AttackControllerTest {

    @Mock
    private HitService mockService;

    @InjectMocks
    private AttackController sut;

    @Test
    void getCommands() {
        // Act
        final ResponseEntity<String> commands = sut.getCommands();

        // Assert
        assertThat(commands.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(commands.getBody()).contains("try");
    }

    @Test
    void getPunchForUser() {
        // Arrange
        final String user = "test";
        when(mockService.throwPunchForUser(anyString()))
                .thenReturn(PunchDto.builder()
                        .owner(user)
                        .build());
        // Act
        final ResponseEntity<PunchDto> punch = sut.getPunchForUser(user);

        // Assert
        assertThat(punch.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(punch.getBody()).isNotNull();
        assertThat(punch.getBody().getOwner()).isEqualTo(user);
    }
}
