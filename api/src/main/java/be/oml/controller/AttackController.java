package be.oml.controller;

import be.oml.dto.PunchDto;
import be.oml.service.HitService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/attack")
public class AttackController {

    private final HitService hitService;

    @GetMapping("/{user}/punch")
    public ResponseEntity<PunchDto> getPunchForUser(@PathVariable @NotBlank final String user) {
        log.info("method getPunchForUser called");
        return ResponseEntity.ok(hitService.throwPunchForUser(user));
    }

    @GetMapping
    public ResponseEntity<String> getCommands() {
        return ResponseEntity.ok("try /user/punch");
    }
}
